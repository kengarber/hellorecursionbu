public class Test {

	public static int factorialI(int x){
		// loop invariant
		int factorial = 1;
		int counter = 1;
		while (counter < x) {
			factorial *= ++counter;
		}
		return factorial;
	}

	public static int factorial(int x) {
		if (x == 0) {
			return 1;
		}
		else {
			return x * factorial(x-1);
		}
	}

	public static void main(String args[]){
		System.out.println("Testing here again!");
		System.out.println("Factorial of 5: " + factorial(5));
		System.out.println("FactorialI of 5: " + factorialI(5));
	}
}
